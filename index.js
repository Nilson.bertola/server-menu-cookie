const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

const jwt = require("jsonwebtoken");

const SECRET = "DELICIA";

const menus = {
  admin: [
    {
      label: "Inicio",
      url: "/",
      component: "Home"
    },
    {
      label: "config",
      url: "/config",
      component: "Config",
      submenu: [
        {
          label: "senha",
          url: "/senha",
          component: "Senha"
        }
      ]
    }
  ],
  user: [
    {
      label: "Inicio",
      url: "/",
      component: "Home"
    },
    {
      label: "Chamados",
      url: "/salva-nois",
      component: "Chamados"
    }
  ]
};

// SERVER PRESETS
const corsConf = {
  origin: origin => origin,
  credentials: true
};

app.use("*", cors(corsConf));
app.options("*", cors(corsConf));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.listen(3001, console.log("live on 3001"));

app.get("/", (_, resp) => resp.send("OK"));

app.post("/login", (req, res) => {
  const { login } = req.body;
  const token = jwt.sign({ menus: menus[login] }, SECRET);
  res
    .cookie("xApiCcellToken", token)
    .status(200)
    .send({
      message: "Done."
    });
});

app.get("/menus", (req, res) => {
  const incommingToken = req.cookies.xApiCcellToken || null;
  console.log(incommingToken);
  const { menus } = jwt.decode(incommingToken, SECRET);
  console.log(menus);
  res.json(menus);
});
